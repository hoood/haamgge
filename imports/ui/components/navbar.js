import { Template } from 'meteor/templating';
import './navbar.html';
import './facebook-login.js';

Template.navbar.events({
    'click #nav_log'(event){
        Session.set('headbox', 'headbox_main');
        Session.set('controlbox', 'controlbox_main');
        Session.set('messagebox', 'messagebox');
        Session.set('isMain','true');
    },
});
