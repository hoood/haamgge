import './facebook-login.html';

Template.login.events({
    'click #facebook-login': function(event) {
        Meteor.loginWithFacebook({requestPermissions: ['email']}, 
            (err) => {
                if(err){
                }
                else{
                }}
        );
    },
 
    'click #logout': function(event) {
        Meteor.logout(function(err){
            if (err) {
                throw new Meteor.Error("Logout failed");
            }
        })
    }
});

Template.login.helpers({
    profile_img(){
        return Meteor.user().profile.picture;
    },
});
