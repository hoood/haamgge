import {Meteor} from 'meteor/meteor';
import {Countrys} from '../countrys.js';

Meteor.publish('countrys.public',function countrysPublic(){
    return Countrys.find({},{
        fields: Countrys.publicFields,
    });
});

