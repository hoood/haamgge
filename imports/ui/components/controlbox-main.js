import { Template } from 'meteor/templating';
import { Session } from 'meteor/session';
import { ReactiveDict } from 'meteor/reactive-dict';
import { Countrys } from '../../api/countrys/countrys.js';
import './controlbox-main.html'

Template.controlbox_main.onCreated(function controlboxOnCreated(){
    Session.set('city_name','프랑스');
});
Template.controlbox_main.rendered=function() {
    $('#datepicker').datepicker({
        startDate: "local",
        format: "yyyy.mm.dd",
        orientation: "bottom left",
        todayHighlight: true
    });
    var date = new Date();
    date = date.getFullYear() +"." + (date.getMonth()+1) + "." + date.getDate();
    $('#datepicker').val(date);
}
Template.controlbox_main.events({
    'click a'(event){
        $("select").selectpicker("refresh");
        return Session.set('city_name', $('span.filter-option')[0].innerText);
    },
    'click #searchbutton'(event){
        Session.set('headbox', 'headbox_search');
        Session.set('controlbox', 'controlbox_search');
        Session.set('messagebox', 'messagebox');
        Session.set('isMain', 'false');
        date = $('#datepicker').val().split('.'); 
        var search_query = {
            eventAt : new Date(date[0],date[1]-1,date[2]),
            country : $('span.filter-option')[0].innerText,  
            city : $('span.filter-option')[1].innerText, 
            item : '여행',
        };
        Session.set('search_query',search_query);
    },
    'keyup #datepicker, focusout #datepicker'(event){
       if($('#datepicker').val() ==""){ 
        var date = new Date();
        date = date.getFullYear() +"." + (date.getMonth()+1) + "." + date.getDate();
        $('#datepicker').val(date);
       }
    },
});
Template.controlbox_main.helpers({
    countrys(){
        Countrys.find().count();
        $("select").selectpicker("refresh");
        $(".selectpicker").selectpicker();
        return Countrys.find();
    },
    citys(){
        $("select").selectpicker("refresh");
        Countrys.find({name:city_name}).count();
        setTimeout(function(){
            $("select").selectpicker("refresh");
        },100);
        var city_name = Session.get('city_name');
        if( !Countrys.findOne({name:city_name})){
            return [];
        }
        return Countrys.findOne({name:city_name}).citys;
    },
});
