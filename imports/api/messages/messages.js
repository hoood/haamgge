import {Meteor} from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import {SimpleSchema} from 'meteor/aldeed:simple-schema';



class MessageCollection extends Mongo.Collection {
  insert(doc, callback) {
    const ourDoc = doc;
    ourDoc.createdAt = ourDoc.createdAt || new Date();
    const result = super.insert(ourDoc, callback);
    return result;
  }
}
export const Messages = new MessageCollection('messages');

Messages.deny({
    insert() {return true;},
    update() {return true;},
    remove() {return true;},
});

Messages.schema = new SimpleSchema({
    _id:{
        type: String,
        regEx: SimpleSchema.RegEx.Id,
    },
    text:{
        type: String,
        max: 100,
    },
    eventAt:{
        type: Date,
    },
    createdAt:{
        type: Date,
        denyUpdate: true,
    },
    country:{
        type:String,
    },
    city:{
        type:String,
    },
    item:{
        type:String,
    },
    owner_id:{
        type:String,
        regEx: SimpleSchema.RegEx.Id,
    },
    owner_name:{
        type: String,
    },
    fb_link:{
        type:String,
    },
    profile_img:{
        type:String,
    },
});
Messages.attachSchema(Messages.schema);

Messages.publicFields = {
    text: 1,
    eventAt: 1,
    country: 1,
    city: 1,
    item: 1,
    owner_id: 1,
    owner_name: 1,
    fb_link: 1,
    profile_img: 1,
};

Meteor.methods({
    'messages.insert'(doc){
        check(doc, {
            text:String,
            eventAt: Date,
            createdAt: Date,
            country: String,
            city: String,
            item: String,
            owner_id:String,
            owner_name:String,
            fb_link:String,
            profile_img:String,
        });
        if (! this.userId) {
            throw new Meteor.Error('not-authorized');
        }
        Messages.insert(doc);
    },
});
