import { Template } from 'meteor/templating';
import { Mongo } from 'meteor/mongo';
import { Session } from 'meteor/session';
import { Messages } from '../../api/messages/messages.js';
import './messagebox.html'

Template.messagebox.onCreated(function bodyOnCreated(){
});


Template.registerHelper('formatDate', function(date) {
      return moment(date).format('MM/DD');
});

Template.messagebox.helpers({
    messages(){
        if(Session.get('isMain') == 'true'){
            return Messages.find({},{sort:{createdAt:-1}});
        }
        if(Session.get('isMain') == 'false'){
            search_query = Session.get('search_query');
            eventAt_next = new Date(search_query.eventAt.getTime());
            eventAt_next.setDate(eventAt_next.getDate()+1);
            return Messages.find({
                eventAt: {"$gte": search_query.eventAt,
                            "$lt": eventAt_next,
                        },
                country:search_query.country,
                city:search_query.city,
                item:search_query.item,
            },{sort:{createdAt:-1}});
        }
    },
    isMain(){
        if(Session.get('isMain') == 'true'){
            return true;
        }
        else {
            return false;
        }
    
    },
});

Template.messagebox.events({
});
