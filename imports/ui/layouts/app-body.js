import { Template } from 'meteor/templating';
import { Mongo } from 'meteor/mongo';
import { ReactiveDict } from 'meteor/reactive-dict';
import { Countrys } from '../../api/countrys/countrys.js';
import { Session } from 'meteor/session';
import './app-body.html';
import '../components/navbar.js';
import '../components/footer.js';
import '../components/headbox-main.js';
import '../components/headbox-search.js';
import '../components/controlbox-main.js';
import '../components/controlbox-search.js';
import '../components/messagebox.js';

Template.App_body.onCreated(function bodyOnCreated(){
    this.state = new ReactiveDict();
    this.subscribe('countrys.public');
    Meteor.subscribe('messages.public');
    Session.set('headbox', 'headbox_main');
    Session.set('controlbox', 'controlbox_main');
    Session.set('messagebox', 'messagebox');
    Session.set('isMain', 'true');
});
//const Countrys = new Mongo.Collection('countrys');
Template.App_body.rendered=function() {
}
Template.App_body.events({
});
Template.App_body.helpers({
    headbox(){
        return Session.get('headbox');
    },
    controlbox(){
        return Session.get('controlbox');
    },
    messagebox(){
        return Session.get('messagebox');
    }
});
