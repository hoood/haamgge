import { Meteor } from 'meteor/meteor';
import { Messages } from '../messages.js';
import {check} from 'meteor/check';

Meteor.publish('messages.public', function messagePublic() {
    var eventAt = new Date();
    eventAt.setDate(eventAt.getDate()-1);
    return Messages.find({
        eventAt: {"$gte": eventAt,
        },
    },{
        fields: Messages.publicFields,
    });
});
