import { Template } from 'meteor/templating';
import { Mongo } from 'meteor/mongo';
import { Session } from 'meteor/session';
import { Messages } from '../../api/messages/messages.js';
import './controlbox-search.html'

Template.controlbox_search.onCreated(function bodyOnCreated(){
    search_query = Session.get('search_query');
    this.subscribe('messages.public',search_query);
});

Template.controlbox_search.helpers({
    message_default(){
        switch(Session.get('search_query').item){
            case '여행':
                return "같이 다닐 분 구합니다. 메세지 주세요~";
            case '밥':
                return "밥 같이 드실 분 구합니다. 메세지 주세요~";
            case '맥주':
                return "맥주 한 잔 하실 분 구합니다. 메세지 주세요~";
            case '야경':
                return "야경 보실 분 구합니다. 메세지 주세요~";
        };
    },
});


var span = $('<span>').css('display','none').css('word-break','break-all').appendTo('body').css('visibility','hidden');

Template.controlbox_search.events({
    'click #message_submit_button'(event){
        text = $('#message_input').val()
        search_query = Session.get('search_query');
        if(text.length<100){
            doc = {
                text: text,
                eventAt: search_query.eventAt,
                createdAt: new Date(),
                country: search_query.country,
                city: search_query.city,
                item: search_query.item,
                owner_id: Meteor.userId(),
                owner_name: Meteor.user().profile.name,
                fb_link: Meteor.user().profile.fb_link,
                profile_img: Meteor.user().profile.picture,
            };
        Meteor.call('messages.insert',doc);
        }
        else{
            alert("100자이내여야합니다.");
        }
    },
    'input textarea'(event){
console.log("input");
        var text = $(event.currentTarget).val();      
        span.text(text);      
        $(event.currentTarget).height(text.length>0 ? span.height() : '20px');
    },
    'focus textarea'(event){
        function initSpan(textarea){
            span.text(textarea.text()).width(textarea.width()).css('font',textarea.css('font'));
        }
        initSpan($(event.currentTarget));
    },
    'keypress textarea'(event){
console.log("key");
        var key = event;
        var text = $(event.currentTarget).val();      
        span.text(text);      
        $(event.currentTarget).height(text ? span.height() : '35px');
        console.log(key.which);
        if(key.which == 13) key.preventDefault();
    },

});
