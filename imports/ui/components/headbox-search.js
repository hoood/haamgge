import {Template} from 'meteor/templating';
import {Session} from 'meteor/session';
import {Countrys } from '../../api/countrys/countrys.js';
import './headbox-search.html'

var change_language = function(word){
    switch(word){
        case '여행':
            return 'travel';
        case '밥':
            return 'meal';
        case '맥주':
            return 'beer';
        case '야경':
            return 'nightview';
        case 'travel':
            return '여행';
        case 'meal':
            return '밥';
        case 'beer':
            return '맥주';
        case 'nightview':
            return '야경';
        //Countrys
        case '이탈리아':
            return 'Italy';
        case '스페인':
            return 'Spain';
        case '프랑스':
            return 'France';
        case '스위스':
            return 'Swiss';
        case '영국':
            return 'United Kingdom';
        case '헝가리':
            return 'Hungary';
        case '오스트리아':
            return 'Austria';
        case '독일':
            return 'Germany';
        case '포르투갈':
            return 'Portugal';
        case '체코':
            return 'Czech Republic';
        //citys
        case '모든도시':
            return 'All';
        case '로마':
            return 'Rome';
        case '피렌체':
            return 'Florence';
        case '베니스':
            return 'Venice';
        case '남부도시':
            return 'Southern Italy';
        case '바티칸':
            return 'Vatican';
        case '밀라노':
            return 'Milano';
        case '바르셀로나':
            return 'Barcelona';
        case '그라나다':
            return 'Granada';
        case '세비아':
            return 'Sevilla';
        case '마드리드':
            return 'Madrid';
        case '파리':
            return 'Paris';
        case '니스':
            return 'Nice';
        case '융프라우':
            return 'Jungfrau';
        case '인터라켄':
            return 'Interlaken';
        case '루체른':
            return 'Luzern';
        case '베른':
            return 'Bern';
        case '런던':
            return 'London';
        case '세븐시스터즈':
            return 'Seven Sisters';
        case '부다페스트':
            return 'Budapest';
        case '비엔나':
            return 'Wien';
        case '할슈타트':
            return 'Hallstatt';
        case '잘츠부르크':
            return 'Salzburg';
        case '뮌헨':
            return 'Munchen';
        case '베를린':
            return 'Berlin';
        case '프랑크푸르트':
            return 'Frankfurt';
        case '드레스덴':
            return 'Dresden';
        case '리스본':
            return 'Lisbon';
        case '포르투':
            return 'Porto';
        case '프라하':
            return 'Praha';
        case '체스키':
            return 'Cesky';
    }
}
var button_refresh = function(){
    $('.button_default').removeClass('button_active');
    item = change_language(Session.get('search_query').item)
    $('#' + item +'_button').addClass('button_active');
    $('textarea').height('20px');
}

Template.headbox_search.rendered = function(){
    button_refresh();
   
};
Template.headbox_search.events({
    'click .button_default'(event){
        var search_query = Session.get('search_query');
        search_query.item = change_language(event.target.id.split('_')[0]);
        Session.set('search_query',search_query);
        Meteor.subscribe('messages.public');
        button_refresh();
    }
});

Template.headbox_search.helpers({
    city(){
        search_query = Session.get('search_query');
        if(search_query.city == '모든도시'){
            return search_query.country;
        }
        else{
            return search_query.city;
        }
    },
    city_en(){
        search_query = Session.get('search_query');
        city_en = change_language(search_query.city);
        return city_en;
    },
    country_en(){
        search_query = Session.get('search_query');
        return change_language(search_query.country);
    },
    item_num(){
        switch(Session.get('search_query').item){
            case '여행':
                return "1";
            case '밥':
                return "2";
            case '맥주':
                return "3";
            case '야경':
                return "4";
        };
    },
    item(){
        switch(Session.get('search_query').item){
            case '여행':
                return "여행 같이 다닐 분?";
            case '밥':
                return "밥 같이 드실 분?";
            case '맥주':
                return "맥주 한 잔 하실 분?";
            case '야경':
                return "야경 같이 보실 분?";
        };
    },
});
