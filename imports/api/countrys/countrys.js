import {Meteor} from 'meteor/meteor';
import {SimpleSchema} from 'meteor/aldeed:simple-schema';
import {Mongo} from 'meteor/mongo';

export const Countrys = new Mongo.Collection('countrys');

Countrys.deny({
    insert() {return true;},
    update() {return true;},
    remove() {return true;},
});

Countrys.schema = new SimpleSchema({
    _id: {
        type: String,
        regEx: SimpleSchema.RegEx.Id
    },
    name: {
        type:String
    },
    name_en: {
        type:String
    },
    citys: {
        type: [Object],
        minCount: 1
    },
    "citys.$.name": {
        type: String
    },
    "citys.$.name_en": {
        type: String
    }
});

Countrys.attachSchema(Countrys.schema);

Countrys.publicFields = {
    name: 1,
    name_en: 1,
    citys: 1,
}
